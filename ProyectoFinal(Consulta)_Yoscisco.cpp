/*
	Name: Proyecto Final - Consulta de Red Social Escolar
	Copyright: UJP
	Author: Francisco Everardo Dan Eumby Hernandez Lopez y Yosef Herrera Cazarin
	Date: 10/10/20 16:27
	Description: Estructura que consulta los datos de una red social para alumnos de un archivo existente.
*/

#include <stdio.h>
#include <stdlib.h>
int main(){
/*Mensajes iniciales*/
cout<<"\nYoscisco (c) [version 1010.2020]\nTodos los derechos reservados.\n ";
cout<<"\nBienvenido al la Consulta de la Red Social Escolar\n";
FILE *nombre;
int c;
nombre = fopen("RedSocial2.txt", "r");
if (nombre == NULL){
printf("El archivo no existe \n");
exit (EXIT_FAILURE);
}else{
do{
c = getc(nombre); /* Obtiene un car´acter del archivo */
putchar(c); /* Lo despliega en pantalla y continua... */
}while (c != EOF); /* hasta encontrar EOF (el final del archivo)*/
}
fclose(nombre);
return EXIT_SUCCESS;
}

